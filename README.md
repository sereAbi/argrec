# README #

# Description

This software computes requirements reconciliation by exploiting conarg [http://www.dmi.unipg.it/~bista/tt/conarg/] for calculating the extensions (hardcoded: "preferred").
More precisely, The ArgREC system considers the following relationships:
  - AND-decomposition: the decomposition of a super-goal into a set of sub-goals is an AND-decomposition if and only if all sub-goals must be satisfied in order to satisfy the super-goal.
  - OR-decomposition: the decomposition of a super-goal into a set of sub-goals is an OR-decomposition if and only if satisfying one of the sub-goals is sufficient for satisfying the super-goal.
  - Conflict dependency: a conflict dependency exists between two goals if the satisfaction of one goal entirely excludes the satisfaction of the other goal, and vice versa.
  - Require dependency: a goal G1 is related to a goal G2 by a require dependency if the satisfaction of the goal G2 is a prerequisite for satisfying goal G1.
  - Goal equivalence: an equivalence dependency exists between two goals if the satisfaction of one goal implies the satisfaction of the other goal.

# Dependencies
- Jung2 graph libraries
- Conarg

Note: conarg path is hardcoded in the MainClass.

# Input file

The input file should be in the following format:

value(g1,1).
value(g2,1).
value(g3,1).
g3 conflict g1.
g1 equivalence g2.

Relations accepted values: conflict, equivalence, require, and, or

# Results

Results are placed in a ./results folder containing the complete graph and pictures of the calculated solutions.